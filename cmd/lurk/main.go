package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

func main() {

	if len(os.Args) < 2 {
		fmt.Println("no input file given")
		os.Exit(1)
	}

	b, err := ioutil.ReadFile(filepath.Clean(os.Args[1]))

	if err != nil {
		fmt.Printf("unable to read file '%s'\n", os.Args[1])
		os.Exit(1)
	}

	fmt.Println(string(b))
}
